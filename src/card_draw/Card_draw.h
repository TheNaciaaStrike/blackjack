#ifndef CARD_DRAW_H
#define CARD_DRAW_H

#include <Arduino.h>



class CD
{
    public:
        unsigned long rngSeed;
        byte hand[2][6];
        unsigned long playerMoney;
        int state;
        CD (long bet);
        void needToGetBetterSeed(unsigned long NewSeed);
        void createDeck();
        void makebank();
        void deal();
        void cardSum();
        void bet(unsigned long pbet);
        bool blackjack(bool BONUS);
        void phit();
        void dhit();
        void stand();
        void bank();
        void witchCardSumIsBetterforPlayer();
        void witchCardSumIsBetterforDealer();
        void winner(bool withBonus, unsigned long pbet);
    private:
        bool gameOn;
        byte deck[52];
        byte randomCard;
        byte suits;
        unsigned long pbet;
        byte phand;
        byte dhand;
        byte playedCardSum[2][2];
        byte cardSums[2];
};

#endif