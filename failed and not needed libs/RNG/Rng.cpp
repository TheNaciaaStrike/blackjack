#include "Rng.h"

RNG::RNG (int pinNumber)
{
    randomSeed(analogRead(A0));
    rngseed = random();
}

void RNG::createNewSeed ()
{
    randomSeed(analogRead(A0));
}

void RNG::createNewNumber()
{
    rngseed = random();
}

uint32_t RNG::showSeed(unsigned long showS)
{
    return rngseed;
}